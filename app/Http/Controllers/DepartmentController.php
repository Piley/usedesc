<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests\DepartmentRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DepartmentController extends Controller
{
    public function __construct(Department $department, User $user)
    {
        $this->department = $department;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = $this->department->latest()->paginate(10);
        return view('department.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = $this->user->all();
        return view('department.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        $data = $request->all();
        $data['logo'] = $request->file('logo')->store('logo');
        $department = $this->department::create($data);
        $department->users()->sync($request->users);

        return redirect()->route('department.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!$department = $this->department->find($id))
            return redirect()->back();

        return view('department.show', compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$department = $this->department->find($id))
            return redirect()->back();
        $users = $this->user->all();

        $selected_users = Arr::pluck($department->users()->get()->toArray(), 'id');

        return view('department.edit', [
            'users' => $users,
            'selected_users' => $selected_users,
            'department' => $department,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, $id)
    {
        if (!$department = $this->department->find($id))
            return redirect()->back();

        $department->update($request->all());
        $department->users()->sync($request->users);

        return redirect()->route('department.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$department = $this->department->find($id))
            return redirect()->back();

        $department->delete();
        return redirect()->route('department.index');
    }
}

@extends('layouts.app')
@section('title', "Edit department {$department->id}")
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Edit department {{$department->id}}</h1>
                <form action="{{ route('department.update', $department->id) }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    @include('department._partials.form')
                    <div class="form-group">
                        <h2>Users</h2>
                        @foreach($users as $user)
                            <p>


                                <input type="checkbox" value="{{ $user->id }}" name="users[]" @if(in_array($user->id, $selected_users)) checked @endif>
                                <label for="{{ $user->id }}">{{ $user->name }} <a
                                            href="mailto:{{ $user->email }}">{{ $user->email }}</a></label>
                            </p>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
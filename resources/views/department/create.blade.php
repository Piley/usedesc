@extends('layouts.app')
@section('title', 'Create new department')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Create department</h1>
                <form action="{{ route('department.store') }}" method="post" enctype="multipart/form-data">
                    @include('department._partials.form')
                    <div class="form-group">
                        <h2>Users</h2>
                        @foreach($users as $user)
                            <p>
                                <input type="checkbox" value="{{ $user->id }}" name="users[]">
                                <label for="{{ $user->id }}">{{ $user->name }} <a
                                            href="mailto:{{ $user->email }}">{{ $user->email }}</a></label>
                            </p>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
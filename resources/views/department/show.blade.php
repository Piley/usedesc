@extends('layouts.app')
@section('title', "Show department {$department->id}")
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Show department {{ $department->id }}</h1>
                <p>Name: {{ $department->name }}</p>
                <p>Email: {{ $department->description }}</p>
            </div>
        </div>
    </div>
@endsection
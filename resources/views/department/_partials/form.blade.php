@include('includes.alerts')
@csrf
<div class="form-group">
    <input value="{{ $department->name ?? old('name') }}" class="form-control" type="text" name="name" placeholder="Name">
</div>
<div class="form-group">
    <input value="{{ $department->description ?? old('description') }}" class="form-control" type="text" name="description" placeholder="Description">
</div>
<div class="form-group">
    <label for="logo">Logo</label>
    <input class="form-control" type="file" name="logo">
</div>
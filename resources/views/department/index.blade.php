@extends('layouts.app')
@section('title', 'List departments')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="display-5">Departments</h1>
                <div class="btn-group mb-3">
                    <a class="btn btn-dark" href="{{ route('department.create') }}">Add new department</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>Logo</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>Users</td>
                        <td colspan=2>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($departments as $department)
                        <tr>
                            <td><img class="img-responsive" style="max-width: 100px" src="{{ url($department->logo) }}" alt=""></td>
                            <td>{{ $department->name }}</td>
                            <td>{{ $department->description }}</td>
                            <td>
                                <ol>
                                    @foreach ($department->users as $user)
                                        <li>{{ $user->name }}</li>
                                    @endforeach
                                </ol>
                            </td>
                            <td>
                                <a href="{{ route('department.edit',$department->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('department.destroy', $department->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $departments->render() }}
            </div>
        </div>
    </div>
@endsection
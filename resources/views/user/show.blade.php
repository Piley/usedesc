@extends('layouts.app')
@section('title', "Show user {$user->id}")
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Show user {{ $user->id }}</h1>
                <p>Name: {{ $user->name }}</p>
                <p>Email: {{ $user->email }}</p>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')
@section('title', "Edit user {$user->id}")
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Edit user {{$user->id}}</h1>
                <form action="{{ route('user.update', $user->id) }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    @include('user._partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection
@include('includes.alerts')
@csrf
<div class="form-group">
    <input value="{{ $user->name ?? old('name') }}" class="form-control" type="text" name="name" placeholder="Name">
</div>
<div class="form-group">
    <input value="{{ $user->email ?? old('email') }}" class="form-control" type="text" name="email" placeholder="Email">
</div>
<div class="form-group">
    <input value="{{ $user->password ?? old('password') }}" class="form-control" type="text" name="password" placeholder="Password">
</div>
<div class="form-group">
    <button type="submit" class="btn btn-success">Сохранить</button>
</div>
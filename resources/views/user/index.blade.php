@extends('layouts.app')
@section('title', 'List users')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="display-5">Users</h1>
                <div class="btn-group mb-3">
                    <a class="btn btn-dark" href="{{ route('user.create') }}">Add new user</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Created At</td>
                        <td colspan=2>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->created_at}}</td>
                            <td>
                                <a href="{{ route('user.edit',$user->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('user.destroy', $user->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $users->render() }}
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')
@section('title', 'Create new user')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Create user</h1>
                <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
                    @include('user._partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection